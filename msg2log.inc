<?php

/**
 * @file
 *   Include file
 *
 * @version
 *
 * @developer
 *   Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Get messages of specified type which passed $rules from $msgs list
 */
function msg2log_get_test_msgs($type, $rules = NULL, $msgs = array()) {
  empty($msgs) ? $msgs = $_SESSION['msg2log_messages'] : NULL;
  foreach ($msgs[$type] as $key => &$message) {
    $output[] = (msg2log_check_rule($message, $type) ? t('PASSED: ') : t('FAILED: ')) . $message;
  }
  return $output;
}

