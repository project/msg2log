<?php

/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @developer
 *   Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Menu callback for the settings form.
 */
function msg2log_settings_form() {
  module_load_include('inc', 'msg2log');

  $form['msg2log'] = array(
    '#type' => 'fieldset',
    '#title' => t('Msg2Log Rules'),
    '#description' => t("If you want to configure those settings for specified roles, configure it on <a href='!url'>Permission</a> Page.", array('!url' => url('admin/user/permissions'))),
    '#collapsible' => TRUE,
  ); 

  /* ERROR MESSAGES */
  foreach (array('error', 'warning', 'status') as $type) {
    $form['msg2log'][$type] = array(
      '#type' => 'fieldset',
      '#title' => t('Rules for %type messages.', array('%type' => $type)),
      '#collapsible' => TRUE,
    ); 

    $form['msg2log'][$type]['msg2log_' . $type . '_regex'] = array(
      '#type' => 'textarea',
      '#title' => t('Log followed %type messages instead of showing them to the user', array('%type' => $type)),
      '#cols' => 60,
      '#rows' => 5,
      '#default_value' => variable_get('msg2log_' . $type . '_regex', implode("\n", msg2log_settings_default_regex_rules($type))),
      '#description' => t('Enter the RegEx rules which messages should be converted to logs.'),
      '#wysiwyg' => FALSE,
    );

    if (!empty($_SESSION['msg2log_messages'][$type])) {
      $form['msg2log'][$type]['msg2log_' . $type . '_regex_test'] = array(
        '#type' => 'textarea',
        '#cols' => 60,
        '#rows' => 10,
        '#attributes' => array('readonly' => 'readonly', 'disabled' => 'disabled'),
        '#value' => implode("\n" ,msg2log_get_test_msgs($type, variable_get('msg2log_' . $type . '_regex', implode("\n", msg2log_settings_default_regex_rules($type))), $_SESSION['msg2log_messages'])),
        '#description' => t('List of messages which passed the rules.'),
        '#wysiwyg' => FALSE,
      );
    }
  }

  $form['#validate'] = array('msg2log_settings_form_validate');

  return system_settings_form($form); 
}

/**
 * Form API callback to validate the settings form.
 */
function msg2log_settings_form_validate($form, &$form_state) {
  $values = &$form_state['values'];

  /* check message rules */
  foreach (array('msg2log_error_regex', 'msg2log_warning_regex', 'msg2log_status_regex') as $var_name) {
    $myerrors = array();
    $rules = preg_split('/\n/', $values[$var_name]);
    foreach ($rules as $line => $rule) {
      $rule = trim($rule);
      if (!empty($rule) && preg_match($rule, '', $matches) === FALSE) {
        $myerrors[] = t("Rule '%rule' on line %line is invalid! Read more: <a href='!url'>preg_match()</a>", array('%rule' => $rule, '%line' => ++$line, '!url' => REGEX_HELP));
      }
    }
    if (!empty($myerrors)) {
      form_set_error($var_name, implode('<br/>', $myerrors)); // show syntax errors
    }
  }
} 
